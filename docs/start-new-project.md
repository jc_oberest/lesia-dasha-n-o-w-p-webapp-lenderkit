# Starting new project

This guide will help you to start a new project and configure it. You need to do several steps:

[TOC]

## Configure IDE (PHPStorm or WebStorm)

* Configure _Settings > Version Control_ sources for all git submodules
* Turn off "Safe write" mode in _Settings > Appearance & Behavior > System Settings"
* On your host system (Linux or Mac OS) turn off file limits with `sudo launchctl limit maxfiles unlimited unlimited`
* Mark folders as "Excluded" (if they exists): `app/dist`, `app/lenderkit`, `api/lenderkit`

## Init repository

* Clone starter repository
* Remove .git folder: `rm -rf .git/`
* Connect your **project repository**
```bash
git init
git remote add origin {path/to/repository}
git add --all
git commit -m "Init project"
git push
```
## Configure Environments
Inside `/app/.env.example`  and `/app/.env` you need to define the following variables:

  * `VUE_APP_API_BASE_URL` (variable is used to define an API reference)
  * `VUE_APP_PAYMENT_GATEWAY` (variable is used to define payment provider)
  * `VUE_APP_PAYMENT_GATEWAY_HAS_ISA` (variable is used to define whether ISA is enabled or not. Used with "Goji" payment provider only)

## Configure Application
Inside `/app/src/config/index.ts` you need to define all project details, such as currency, address, phone and so on.



## Configure Theme
Inside `/app/src/config/theme.config.js` you need to define the project fonts and logos.
If the theme configurator is enabled - `theme.config.js` file will be replaced with the server version,
every time when theme was changed.

If the theme configurator is disabled, then you need to do the following:

* remove WebApp API Server (the `api/` folder)
* remove `theme.config.js` from the `.gitignore` file, remove `theme.config.example.js` file
* open the `docker-compose.example.yml`,`docker-compose.override.yml` and `docker-compose.yml` file and remove the `webapp-api` service
* open `configs/nginx-server.conf` and `configs/nginx-server.example.conf` and remove the `location /webapp-api` (**do not touch the `include`**)

The next step is editing the Makefiles:

**Root Level Makefile**:

* edit the `init` target: remove creating `api/.env` file
* edit the `install` target: remove '${DOCKER_COMPOSE_RUN} ${NODEJS_SERVER_CONTAINER} bash -c "make install SKIP_VENDORS=${SKIP_VENDORS}"'
* edit the `update` target: remove '${DOCKER_COMPOSE_RUN} ${NODEJS_SERVER_CONTAINER} bash -c "make update SKIP_VENDORS=${SKIP_VENDORS}"'
* remove the `NODEJS_SERVER_BASH_CMD` variable
* remove the `api-bash` target
* update the `info` target

**Application Level Makefile**:

* edit the `.PHONY` directive: remove the call of the `themeconfig-init` target
* edit the `install` and `update` targets: remove the call of the `themeconfig-init` target
* remove the `themeconfig-init` target
* update the `info` target

**Environemnts**:

* edit the `environments.yaml` file in the project environments repository: remove `- app/src/config/theme.config.js` line from the `cleanup` step

# CSS Variables
All the CSS variables are stored in the `public/css/default-theme.css`,
so you can edit this file according to your project designs.

## Update README.md

Update readme with project name, default host, URLs and admin user credentials info.
