# Before you start

Before you start working with this code you need to make sure you understand some technologies or tools 
used on this project.

You should be familiar with:

* Server administration:
    * Docker and Docker Compose
    * GNU Make utility
    * _for MacOS users: Vagrant and Virtual Box_

* Web development:
    * HTML5, CSS3, SCSS, PUG, JavaScript\TypeScript basics
    * NPM package manager
    * Vue.js, Vue.js CLI, Vuex, Vue-router, VeeValidate basics

If you're new to this tools, please spent some time to know the basics of them. Otherwise it will be hard to work
with this project. We will try to cover the very very basics for you:

## Docker

The best way is to learn it is to read [official docs](https://docs.docker.com/engine/docker-overview/) and try to play with it by your own.
You will need to know how to use [Docker Compose](https://docs.docker.com/compose/).

You can also search the web for simplified explanations or demo videos like these resources:

* [Docker Tutorial for Beginners - A Full DevOps Course on How to Run Applications in Containers](https://www.youtube.com/watch?v=fqMOX6JJhGo)
* [Docker Simplified: A Hands-On Guide for Absolute Beginners](https://www.freecodecamp.org/news/docker-simplified-96639a35ff36/)

### Super basic Docker intro

In a few words (this is completely wrong, but just for quick understanding) - think about Docker container 
as a separate Linux-based server, which has some pre-installed software.

To setup your application you usually take some pre-defined images for each technology, launch them as containers
 and join them into one network. So for each application you have several small separate servers.
 
These servers are absolutely independent from external Host system, and you can only mount files or bind ports to it.

So to use some software or operate with you need to GET INSIDE the container (open container bash). 

## GNU Make

Initially GNU Make util is used to build executable programs from a source code. You can read more in [Wikipeida](https://en.wikipedia.org/wiki/Make_(software)).
However it is also used to automate some complex processes and simplify CLI usage to running few simple commands.
The main advantage of the Make util - it's already installed in almost any Linux distribution and you don't need 
any additional requirements.

Please read this [simple guide](https://opensource.com/article/18/8/what-how-makefile) to get a better understanding of a Makefile.

To simplify docker operations, installation/updating process the project use GNU Make util, 
configured inside Makefile's. By default our Makefile's executes `info` target, which list all available operations:

```
make
```

There are **3 Makefile's** in the project:

* `/Makefile` or **root level Makefile**.  
This file contains targets to configure your server environment and operate with Docker containers. 
This one should be called **outside** any Docker container (If you're on Mac - inside Vagrant CLI).
* `/app/Makefile` or **application level Makefile**.  
This file contains targets to operate with your JavaScript application. It should be called **inside Node containers** (webapp-app). 
`/app` folder is mounted as a docroot `/var/www/html` to docker node.js containers, so it's the only available Makefile inside the containers. 
In general this Makefile contains application install/update scripts calls.
* `/api/Makefile` or **WebApp API level Makefile**.  
This file contains targets to operate with WebApp API Server.

## Vagrant (for MacOS users)

If you're working on MacOS, then we are strongly recommend to install Virtual Box and Vagrant to run Docker inside the 
Linux Virtual Machine (VM). (Read more in [Requirements](requirements.md) section on how to setup it)

Of course you can use "Docker for Mac", but some Makefile's targets won't work, because MacOS bash has a bit different
commands set and some commands won't work.
Furthermore, Docker for Mac has known speed problems and you will have serious speed issues running project directly
on Mac without the VM.  

So please take your time to know what the [VirtualBox](https://www.virtualbox.org/) is and what is [Vagrant](https://www.vagrantup.com/).
