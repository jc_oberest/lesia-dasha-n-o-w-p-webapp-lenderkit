# Docker Containers

If you run `make init-dev` and keep default configuration inside `docker-compose.yml` and `docker-compose.override.yml`
then after run you will have such containers:

* `webapp-nginx`  - the web-server to serve app static built dist files and processes server api requests and forwards them to api node.js server application
* `webapp-app`    - the application itself
* `webapp-api`    - the application API (uses Node.js)

## Containers Access

To login to a running `webapp-app` container you need to run `bash` command on it:

`docker-compose run -w /var/www/html {service} bash`

To login to `webapp-nginx` and `webapp-api` yo need to use  the `run` utility:

`docker-compose exec -w /var/www/html {service} bash`

For **node.js** you can't use `exec`, because it's not running by default.

## File permissions

Docker has an issue with file permissions. You cann't modify any files created inside docker container
(for example `package-lock.json`).

To fix this you need to run a command to set correct file owner (run on a root level means from MacOS, **not from Vagrant**):

`make chown`

## Helpers
### Root level Makefile

**chown:**
Fix file permissions for files created inside docker container

**nodejs-bash:**
Login to the Node.js container

**api-bash:**
Login to the Node.js API-server bash

**npm-build:**
Runs `npm run build` with node memory limit option

**npm-multisite-build:**
Builds the multisite

**npm-watch:**
Runs build in watch mode with node memory limit option (`npm run watch`)

### Application level Makefile

**chmod:**
Chmod important folders

**dotenv-init:**
Create .env file

**themeconfig-init:**
Create theme.config.js

**gtmconfig-init:**
Create gtm.config.js

**packagejson-init:**
Init package.json

**corelibrary-init:**
Init the core library
