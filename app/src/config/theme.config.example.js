/**
 * @copyright Copyright (c) JustCoded Ltd. All Rights Reserved.
 *    Unauthorized copying of this file, via any medium is strictly prohibited.
 *    Proprietary and confidential.
 *
 * @license https://lenderkit.com/license
 * @see https://lenderkit.com/
 *
 * @package lenderkit-webapp
 */

export default {
  "id": 1,
  "name": "modern",
  "title": "Modern",
  "active": true,
  "assets": {
    "css": {
      "styles": "/css/default-theme.css"
    },
    "images": {
      "logo_light": "/images/logo-light.svg",
      "logo_dark": "/images/logo-dark.svg",
      "logo_icon": "/images/logo-small.svg"
    }
  },
  "variables": {
    "color_scheme": "persian-blue",
    "body_background": "light",
    "body_font_family": "Chivo",
    "headings_font_family": "Roboto",
  }
}
