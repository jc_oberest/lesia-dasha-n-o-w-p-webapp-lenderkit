// @ts-ignore
import themeConfig from '@/config/theme.config';
// @ts-ignore
import gtmConfig from '@/config/gtm.config';

const urlParams = new URLSearchParams(window.location.search.slice(1));
const isPreviewModeEnabled = urlParams.has('preview') && Boolean(urlParams.get('preview'));
const previewToken = urlParams.has('access_token') && String(urlParams.get('access_token'));
const activeTheme = (isPreviewModeEnabled && urlParams.has('theme'))
  ? urlParams.get('theme')
  : themeConfig.name;

export default {
  contactEmail: 'admin@jc.com',
  address: '10 Castle terrace, Dolwyddelon, LL25 0NJ London, UK',
  phone: '+38 (099) 232 22 11',
  title: 'LenderKit WebApp',
  WPSiteUrl: process.env.VUE_APP_WP_SITE_URL || '',
  WPAPIBaseUrl: process.env.VUE_APP_WP_SITE_URL ? `${process.env.VUE_APP_WP_SITE_URL}${'wp-json/wp/v2/'}` : '',

  currency: {
    name: 'GBP',
    symbol: '&#163;',
  },

  locale: 'en',

  fileSize: {
    max: 7000000,
    min: 1024,
  },

  acceptableFormats: {
    registration: /\.(pdf|jpeg|png|jpg|gif)$/i,
  },

  modals: {
    timeout: 5000,
  },

  validation: {
    validity: true,
    delay: 1500,
  },

  cache: {
    dashboardLifetime: 15 * 60 * 1000, // 15 minutes
    offerings: 15 * 60 * 1000, // 15 minutes
  },

  paymentGateway: {
    provider: process.env.VUE_APP_PAYMENT_GATEWAY,
    hasISA: process.env.VUE_APP_PAYMENT_GATEWAY_HAS_ISA === 'true',
  },

  layout: activeTheme,

  previewMode: isPreviewModeEnabled,

  themeConfig,
  gtmConfig,

  previewToken,

  timeZone: {
    utcOffset: 0,
  },

  currentYear: new Date().getFullYear(),

  sentryDSN: process.env.VUE_APP_SENTRY_DSN,
};
