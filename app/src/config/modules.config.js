/* NOTE! This file contains special comments fot autosetup.
  If you add any optional module to webapp application, please surround all the module lines with the next comments
  //module-{same-as-appropriate-backend-module-name}-lines-begin fot lines start
  //module-{same-as-appropriate-backend-module-name}-lines-end fot lines finish
  Note, that modulec-module comments has NO space after //
*/





//module-site-request-begin
import SiteRequestModule from '@siterequest/Installer';
//module-site-request-end
//module-site-donation
import Donation from '@donation/Installer';
//module-site-donation
//module-secondarymarket-lines-begin
import SecondaryMarketModule from '@secondarymarket/Installer';
//module-secondarymarket-lines-end
//module-debt-lines-begin
// import Debt from '@debt/Installer';
//module-debt-lines-end

export const enabledModules = [





//module-secondarymarket-lines-begin
  'secondarymarket',
//module-secondarymarket-lines-end
//module-secondarymarket-debt-lines-begin
  'secondarymarket-debt',
//module-secondarymarket-debt-lines-end


//module-debt-lines-begin
//   'debt',
//module-debt-lines-end
];

export default [




//module-site-request-begin
  SiteRequestModule,
//module-site-request-end
//module-donation-begin
  Donation,
//module-donation-end
//module-secondarymarket-lines-begin
  SecondaryMarketModule,
//module-secondarymarket-lines-end
//module-debt-lines-begin
//   Debt,
//module-debt-lines-end
];
